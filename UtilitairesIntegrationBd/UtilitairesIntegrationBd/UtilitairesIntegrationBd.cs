﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Novacode;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace UtilitairesIntegrationBd
{
    public static class UtilitairesIntegrationBd
    {
        private static string sRlConnexion = "server=172.16.0.159;uid=nicolas;database=rallye;password=lolilol;port=3306";
        private static MySqlConnection rlConnexion = new MySqlConnection(sRlConnexion);
        private static MySqlCommand insertAuteur = new MySqlCommand("Insert into Auteur(nom) values (@p1)", rlConnexion);
        private static MySqlCommand insertEditeur = new MySqlCommand("Insert into Editeur(nom) values(@p1)", rlConnexion);
        private static MySqlCommand insertLivre = new MySqlCommand("Insert into Livre(titre, couverture, idAuteur, idEditeur, idQuizz) values(@p1, @p2, @p3, @p4, @p5)", rlConnexion);
        private static MySqlCommand insertProposition = new MySqlCommand("Insert into Proposition(proposition, solution, idQuestion) values(@p1, @p2, @p3)", rlConnexion);
        private static MySqlCommand insertQuestion = new MySqlCommand("Insert into Question(question, points, idQuizz) values(@p1, @p2, @p3)", rlConnexion);
        private static MySqlCommand insertQuizz = new MySqlCommand("Insert into Quizz(idFiche) values(@p1)", rlConnexion);
        private static MySqlCommand readerLivre = new MySqlCommand("Select id, titre, couverture, idAuteur, idEditeur, idQuizz from Livre", rlConnexion);     //  requête à mettre à jour en fonction des besoins du groupe
        private static MySqlCommand updateLivre = new MySqlCommand("Update Livre set titre=@p1, couverture=@p2 ,idAuteur=@p3, idEditeur=@p4, idQuizz=@p5 where id=@p6", rlConnexion);
        private static MySqlCommand updateProposition = new MySqlCommand("Update Proposition set solution=1 where id=@p1", rlConnexion);

        //  requête pour ExtractionSolution
        private static MySqlCommand selectIdFicheQuizz = new MySqlCommand("Select idFiche from Quizz", rlConnexion);
        private static MySqlCommand selectIdQuizz = new MySqlCommand("Select id from Quizz where idFiche=@p1", rlConnexion);
        private static MySqlCommand selectIdsQuestion = new MySqlCommand("Select id from Question where idQuizz=@p1", rlConnexion);
        private static MySqlCommand selectIdsProposition = new MySqlCommand("Select id from Proposition where idQuestion=@p1", rlConnexion);

        //  requête pour ExtractionQuizz
        private static MySqlCommand selectNomAuteur = new MySqlCommand("Select nom from Auteur", rlConnexion);
        private static MySqlCommand selectIdAuteur = new MySqlCommand("Select id from Auteur where nom=@p1", rlConnexion);
        private static MySqlCommand selectNomEditeur = new MySqlCommand("Select nom from Editeur", rlConnexion);
        private static MySqlCommand selectIdEditeur = new MySqlCommand("Select id from Editeur where nom=@p1", rlConnexion);

        /// <summary>
        /// Constructeur de la classe UtilitairesIntegrationBd
        /// </summary>
        static UtilitairesIntegrationBd()
        {
            //  Paramètres insertAuteur
            insertAuteur.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));    //  nom auteur
            //  Paramètres insertEditeur
            insertEditeur.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));   //  nom éditeur
            //  Paramètres insertLivre
            #region insertLivre
            insertLivre.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));     //  titre
            insertLivre.Parameters.Add(new MySqlParameter("@p2", MySqlDbType.VarChar));     //  couverture
            insertLivre.Parameters.Add(new MySqlParameter("@p3", MySqlDbType.Int32));       //  idAuteur
            insertLivre.Parameters.Add(new MySqlParameter("@p4", MySqlDbType.Int32));       //  idEditeur
            insertLivre.Parameters.Add(new MySqlParameter("@p5", MySqlDbType.Int32));       //  idQuizz
            #endregion
            //  Paramètres insertProposition
            #region insertProposition
            insertProposition.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));   //  proposition
            insertProposition.Parameters.Add(new MySqlParameter("@p2", MySqlDbType.Int32));     //  solution
            insertProposition.Parameters.Add(new MySqlParameter("@p3", MySqlDbType.Int32));     //  idQuestion
            #endregion
            //  Paramètres insertQuestion
            #region insertQuestion
            insertQuestion.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));      //  question
            insertQuestion.Parameters.Add(new MySqlParameter("@p2", MySqlDbType.Int32));        //  points
            insertQuestion.Parameters.Add(new MySqlParameter("@p3", MySqlDbType.Int32));        //  idQuizz
            #endregion
            //  Paramètres insertQuizz
            insertQuizz.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.Int32));   //  idFiche
            //  ParamètresUpdateLivre
            #region UpdateLivre
            updateLivre.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));     //  titre
            updateLivre.Parameters.Add(new MySqlParameter("@p2", MySqlDbType.VarChar));     //  converture
            updateLivre.Parameters.Add(new MySqlParameter("@p3", MySqlDbType.Int32));       //  idAuteur
            updateLivre.Parameters.Add(new MySqlParameter("@p4", MySqlDbType.Int32));       //  idEditeur
            updateLivre.Parameters.Add(new MySqlParameter("@p5", MySqlDbType.Int32));       //  idQuestion
            updateLivre.Parameters.Add(new MySqlParameter("@p6", MySqlDbType.Int32));       //  id  (idLivre)
            #endregion
            //  Paramètres selectIdQuizz
            selectIdQuizz.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.Int32)); //  idFiche
            //  Paramètre selectIdsQuestion
            selectIdsQuestion.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.Int32)); //  idQuizz
            //  Paramètre selectIdsProposition
            selectIdsProposition.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.Int32));  //  idQuestion
            //  Paramètre updateProposition
            updateProposition.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.Int32)); //  idProposition
            //  Paramètre selectIdAuteur
            selectIdAuteur.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar));  //  nom auteur
            //  Paramètre selectIdEditeur
            selectIdEditeur.Parameters.Add(new MySqlParameter("@p1", MySqlDbType.VarChar)); //  nom editeur

        }
        /// <summary>
        /// Test permet de tester les requêtes SQL.
        /// </summary>
        private static void Test()
        {
            //  Test connexion
            rlConnexion.Open();

            //  Test insertAuteur
            insertAuteur.Parameters[0].Value = "nomAuteur";
            insertAuteur.ExecuteNonQuery();

            //  Test insertEditeur
            insertEditeur.Parameters[0].Value = "nomEditeur";
            insertEditeur.ExecuteNonQuery();

            //  Test insertQuizz
            insertQuizz.Parameters[0].Value = 1;
            insertQuizz.ExecuteNonQuery();

            //  Test insertLivre
            insertLivre.Parameters[0].Value = "Eragon";
            insertLivre.Parameters[1].Value = "pathImage";
            insertLivre.Parameters[2].Value = 1;
            insertLivre.Parameters[3].Value = 1;
            insertLivre.Parameters[4].Value = 1;
            insertLivre.ExecuteNonQuery();

            //  Test insertQuestion
            insertQuestion.Parameters[0].Value = "nomQuestion";
            insertQuestion.Parameters[1].Value = 20;
            insertQuestion.Parameters[2].Value = 1;
            insertQuestion.ExecuteNonQuery();

            //  Test insertProposition
            insertProposition.Parameters[0].Value = "nomProposition";
            insertProposition.Parameters[1].Value = 3;
            insertProposition.Parameters[2].Value = 1;
            insertProposition.ExecuteNonQuery();

            MySqlDataReader Rdr = readerLivre.ExecuteReader();
            while (Rdr.Read())
            {
                Console.WriteLine(
                    Rdr["id"].ToString() + " | " +
                    Rdr["titre"].ToString() + " | " +
                    Rdr["couverture"].ToString() + " | " +
                    Rdr["idAuteur"].ToString() + " | " +
                    Rdr["idEditeur"].ToString() + " | " +
                    Rdr["idQuizz"].ToString());
            }
            Rdr.Close();

            //  Test updateLivre
            updateLivre.Parameters[0].Value = "A Song Of Ice and Fire";
            updateLivre.Parameters[1].Value = "pathImage2";
            updateLivre.Parameters[2].Value = 1;
            updateLivre.Parameters[3].Value = 1;
            updateLivre.Parameters[4].Value = 1;
            updateLivre.Parameters[5].Value = 1;
            updateLivre.ExecuteNonQuery();

            Rdr = readerLivre.ExecuteReader();
            while (Rdr.Read())
            {
                Console.WriteLine(
                    Rdr["id"].ToString() + " | " +
                    Rdr["titre"].ToString() + " | " +
                    Rdr["couverture"].ToString() + " | " +
                    Rdr["idAuteur"].ToString() + " | " +
                    Rdr["idEditeur"].ToString() + " | " +
                    Rdr["idQuizz"].ToString());
            }
            Rdr.Close();
            rlConnexion.Close();
        }

        /// <summary>
        /// ViderBase permet de vider les tables Auteur, Editeur, Livre, Question, Quizz, Proposition de la base de données.
        /// </summary>
        public static void ViderBase()
        {
            MySqlCommand deleteProposition = new MySqlCommand("delete from Proposition", rlConnexion);
            MySqlCommand incrementProposition = new MySqlCommand("alter table Proposition auto_increment=1", rlConnexion);
            MySqlCommand deleteQuizz = new MySqlCommand("delete from Quizz", rlConnexion);
            MySqlCommand incrementQuizz = new MySqlCommand("alter table Quizz auto_increment=1", rlConnexion);
            MySqlCommand deleteQuestion = new MySqlCommand("delete from Question", rlConnexion);
            MySqlCommand incrementQuestion = new MySqlCommand("alter table Question auto_increment=1", rlConnexion);
            MySqlCommand deleteLivre = new MySqlCommand("delete from Livre", rlConnexion);
            MySqlCommand incrementLivre = new MySqlCommand("alter table Livre auto_increment=1", rlConnexion);
            MySqlCommand deleteEditeur = new MySqlCommand("delete from Editeur", rlConnexion);
            MySqlCommand incrementEditeur = new MySqlCommand("alter table Editeur auto_increment=1", rlConnexion);
            MySqlCommand deleteAuteur = new MySqlCommand("delete from Auteur", rlConnexion);
            MySqlCommand incrementAuteur = new MySqlCommand("alter table Auteur auto_increment=1", rlConnexion);

            rlConnexion.Open();
            deleteProposition.ExecuteNonQuery();
            deleteQuestion.ExecuteNonQuery();
            deleteLivre.ExecuteNonQuery();
            deleteQuizz.ExecuteNonQuery();
            deleteEditeur.ExecuteNonQuery();
            deleteAuteur.ExecuteNonQuery();
            incrementQuestion.ExecuteNonQuery();
            incrementProposition.ExecuteNonQuery();
            incrementQuizz.ExecuteNonQuery();
            incrementEditeur.ExecuteNonQuery();
            incrementAuteur.ExecuteNonQuery();
            incrementLivre.ExecuteNonQuery();

            rlConnexion.Close();
        }

        /// <summary>
        /// ExtractionPhoto permet de récupérer la photo du document world pour l'ajouter dans un dossier sur BABAORUM.
        /// </summary>
        /// <param name="fiche"></param>
        public static void ExtractionPhoto(string fiche)
        {
            using (DocX document = DocX.Load(fiche))
            {
                foreach (Novacode.Image image in document.Images)
                {
                    try
                    {
                        Stream s = image.GetStream(FileMode.Open, FileAccess.Read);
                        Bitmap b = new Bitmap(s);

                        string nomFichier = Path.GetFileNameWithoutExtension(fiche);
                        b.Save(@"\\BABAORUM\photo\" + nomFichier + ".jpeg", ImageFormat.Jpeg);
                    }
                    catch (Exception e)
                    {
                        // Si il y a un probleme, il faut décommenter la messageBox ci-dessous
                        //MessageBox.Show(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// getIdFiche permet de récupérer l'ID d'une fiche
        /// </summary>
        /// <param name="path"></param>
        /// <returns>int</returns>
        public static int GetIdFiche(string path)
        {
            if (File.Exists(path))
            {
                // Si le fichier existe, on retourne l'ID
                char[] splitChar = new char[] { ' ' };
                string[] laCase = path.Split(splitChar);
                return Convert.ToInt32(laCase[1]);
            }
            else
            {
                // Si le fichier n'existe pas, on retourne -1
                return -1;
            }
        }

        /// <summary>
        /// separer permet de séparer une chaîne de caractères selon un caractère passé en paramètre, 
        /// il retourne les différentes parties de la chaîne de caractère trouvées dans un tableau de string.
        /// </summary>
        /// <param name="mot">mot à couper</param>
        /// <param name="lettreACoupe">tableau de char</param>
        /// <returns></returns>
        private static string[] separer(string mot, char[] lettreACoupe)
        {
            char[] elementACoupe = lettreACoupe;
            string[] elementCoupe = mot.Split(elementACoupe);
            return elementCoupe;
        }

        /// <summary>
        /// ExtractionSolution permet de récupérer les questions et les réponses des quizz.
        /// </summary>
        /// <param name="path"></param>
        public static void ExtractionSolution(string path)
        {
            if (File.Exists(path))
            {
                rlConnexion.Open();
                StreamReader reader = new StreamReader(path);
                string ligne;

                MySqlDataReader readerQuizz = selectIdFicheQuizz.ExecuteReader();
                List<int> lesNumérosFiches = new List<int>();

                while (readerQuizz.Read())
                {
                    lesNumérosFiches.Add(Convert.ToInt32(readerQuizz["idFiche"]));
                }
                readerQuizz.Close();

                while ((ligne = reader.ReadLine()) != null)
                {
                    ligne = ligne.Trim();   //  On enlève les espaces au début et à la fin de la ligne

                    string[] ligneEnDeux = ligne.Split(':');    //  On sépare la ligne en deux au ":"
                    string[] ligneAlpha = ligneEnDeux[0].Split(' ');    //  première partie pour l'idFiche
                    string[] ligneBeta = ligneEnDeux[1].Split('-');     //  deuxième partie pour les propositions
                    string numero = ligneAlpha[1].Substring(2);     //  On récupère juste l'idFiche

                    foreach (int numéroQuizz in lesNumérosFiches)
                    {
                        if (Convert.ToInt32(numero) == numéroQuizz)     //  On teste si la fiche est présente dans la base
                        {
                            selectIdQuizz.Parameters[0].Value = numéroQuizz;
                            MySqlDataReader readerSelectIdQuizz = selectIdQuizz.ExecuteReader();
                            while (readerSelectIdQuizz.Read())
                            {
                                selectIdsQuestion.Parameters[0].Value = Convert.ToInt32(readerSelectIdQuizz["id"]);
                            }
                            readerSelectIdQuizz.Close();

                            MySqlDataReader readerQuestions = selectIdsQuestion.ExecuteReader();

                            List<int> idsQuestion = new List<int>();

                            while (readerQuestions.Read())
                            {
                                idsQuestion.Add(Convert.ToInt32(readerQuestions["id"]));
                            }
                            readerQuestions.Close();
                            int index = 0;
                            foreach (int i in idsQuestion)
                            {
                                selectIdsProposition.Parameters[0].Value = i;
                                List<int> idsProposition = new List<int>();
                                MySqlDataReader readerIdsProposition = selectIdsProposition.ExecuteReader();
                                while (readerIdsProposition.Read())
                                {
                                    idsProposition.Add(Convert.ToInt32(readerIdsProposition["id"]));
                                }
                                readerIdsProposition.Close();

                                string lettres = ligneBeta[index].Trim();
                                string[] reponses = lettres.Split('.');
                                lettres = reponses[1].Replace(',', ' ');    //  Remplace les virgules par des espaces
                                lettres = lettres.Replace("et", " ");       //  Remplace les "et" par des espaces
                                lettres = lettres.Replace("   ", " ");      //  Remplace les triples espaces par un seul espace
                                lettres = lettres.Replace("  ", " ");       //  Remplace les doubles espaces par un seul espace
                                lettres = lettres.Replace("\t", " ");       //  Remplace les "tabulations" par un seul espace
                                lettres = lettres.Trim();
                                reponses = lettres.Split(' ');
                                //Console.WriteLine(reponses.Count());
                                for (int j = 0; j < reponses.Count(); j++)
                                {
                                    char c = Convert.ToChar(reponses[j]);
                                    updateProposition.Parameters[0].Value = idsProposition[((int)c) - 97];
                                    updateProposition.ExecuteNonQuery();
                                }
                                readerIdsProposition.Close();
                                index++;
                            }
                            break;
                        }
                    }
                }
            }
            rlConnexion.Close();
        }

        /// <summary>
        /// lesFiches permet de récupérer tous les noms de fichier dans une liste de string. 
        /// </summary>
        /// <param name="path"></param>
        /// <returns>List(String)</returns>
        private static List<string> lesFiches(string path)
        {
            List<string> lesFiches = new List<string>();
            DirectoryInfo dossier = new DirectoryInfo(path);

            foreach (FileInfo fichier in dossier.GetFiles())
            {
                string[] ligneEnDeux = fichier.Name.Split(' ');
                if (fichier.Extension == ".docx" && ligneEnDeux[0] == "Fiche")
                {
                    lesFiches.Add(fichier.Name);
                }
            }
            return lesFiches;
        }

        /// <summary>
        /// IntegrationFiches permet d'appeler les méthodes ViderBase, ExtractionQuizz et ExtractionPhoto.
        /// </summary>
        /// <param name="path"></param>
        public static void IntegrationFiches(string path, List<String> lesFiches)
        {
            List<string> fiches = lesFiches;
            //DirectoryInfo dossier = new DirectoryInfo(path);

            //fiches = lesFiches(path);
            ViderBase();
            foreach (string fiche in fiches)
            {
                ExtractionQuizz(path + fiche);
                ExtractionPhoto(path + fiche);
            }
        }

        /// <summary>
        /// Extrait le Titre, l'Auteur, l'Editeur, et la liste des Questions
        /// </summary>
        /// <param name="fiche"></param>
        public static void ExtractionQuizz(string fiche)
        {
            rlConnexion.Open();

            List<string> ListQuestions = new List<string>();

            string titre = "";
            string auteurEditeur = "";
            string auteur = "";
            string editeur = "";

            int idQuestion = 0;
            int idQuizz = 0;
            int idFiche = GetIdFiche(fiche);
            List<string> myDoc = new List<string>();

            //  Load document word
            DocX document = DocX.Load(fiche);

            int i = 0;  //  ligne
            string etape = "debut";
            while (document.Paragraphs[i].Text != "QUESTION")
            {
                if (document.Paragraphs[i].Text != "")
                {
                    switch (etape)
                    {
                        case "debut":
                            if (document.Paragraphs[i].Text.Contains("Nom") == true)    //  la chaine "nom" est trouvée
                            {
                                etape = "titre";    //  On passe au cas suivant
                            }
                            break;

                        case "titre":
                            //  On récupère le titre du livre
                            titre = document.Paragraphs[i].Text;
                            titre = titre.Trim();
                            etape = "auteurEditeur";    //  On passe au cas suivant
                            break;

                        case "auteurEditeur":
                            if (document.Paragraphs[i].Text.Contains("de"))
                            {
                                auteurEditeur = document.Paragraphs[i].Text; // Récupère l'auteur et l'éditeur dans une seule variable

                                // enlever certains éléments de la chaine de caractère contenant l'auteur et l'éditeur
                                if (auteurEditeur.Contains("de"))   //  enlève le "de" au début de la chaîne
                                {
                                    auteurEditeur = auteurEditeur.Trim(); // Retire les espaces vides au début
                                    auteurEditeur = auteurEditeur.Remove(0, 2); // Retire la chaine de caractere "de"
                                    auteurEditeur = auteurEditeur.Trim();   //  Retire l'espace restant (entre le "de" et le nom de l'auteur)
                                }
                                if (auteurEditeur.Contains('–'))
                                {
                                    char[] split = new char[] { '–' };
                                    string[] resultat = separer(auteurEditeur, split); // Sépare l'auteur de l'éditeur
                                    auteur = resultat[0]; // Récupère l'auteur
                                    editeur = resultat[1]; // Récupère l'éditeur
                                    editeur = editeur.Remove(0, 1); // Retire les espaces vides au début
                                }
                                else if (auteurEditeur.Contains('-'))
                                {
                                    char[] split = new char[] { '-' };
                                    string[] resultat = separer(auteurEditeur, split);
                                    auteur = resultat[0]; // Récupère l'auteur
                                    editeur = resultat[1]; // Récupère l'éditeur
                                    editeur = editeur.Remove(0, 1); // Retire les espaces vides au début
                                }

                                //  On gère les doublons au niveau des auteurs
                                MySqlDataReader readerNomsAuteur = selectNomAuteur.ExecuteReader();
                                List<string> lesNomsAuteurs = new List<string>();
                                while (readerNomsAuteur.Read())
                                {
                                    lesNomsAuteurs.Add(Convert.ToString(readerNomsAuteur["nom"]));
                                }
                                readerNomsAuteur.Close();

                                int idAuteur = 0;
                                bool okNomAuteur = false;

                                //  On boucle sur les noms des auteurs déjà insérés dans la base
                                foreach (string nom in lesNomsAuteurs)
                                {
                                    //  Si l'auteur du livre de la fiche est déjà présent dans la base
                                    if (auteur == nom)
                                    {
                                        okNomAuteur = true; //  On passe le bool à true
                                        break;
                                    }
                                }

                                //  Si le bool est à true, on récupère l'id de l'auteur
                                if (okNomAuteur == true)
                                {
                                    selectIdAuteur.Parameters[0].Value = auteur;
                                    MySqlDataReader readerIdAuteur = selectIdAuteur.ExecuteReader();
                                    while (readerIdAuteur.Read())
                                    {
                                        idAuteur = Convert.ToInt32(readerIdAuteur["id"]);   //  On récupère l'id de l'auteur déjà inséré
                                    }
                                    readerIdAuteur.Close();
                                }
                                //  Sinon on insert l'auteur dans la base
                                else
                                {

                                    // insert table Auteur
                                    insertAuteur.Parameters[0].Value = auteur;
                                    insertAuteur.ExecuteNonQuery();
                                    idAuteur = Convert.ToInt32(insertAuteur.LastInsertedId);    //  récupère l'id de l'auteur que l'on vient d'insérer
                                }
                                //  Fin doublons Auteur

                                //  On gère les doublons au niveau des éditeurs
                                MySqlDataReader readerNomsEditeur = selectNomEditeur.ExecuteReader();
                                List<string> lesNomsEditeurs = new List<string>();
                                while (readerNomsEditeur.Read())
                                {
                                    lesNomsEditeurs.Add(Convert.ToString(readerNomsEditeur["nom"]));
                                }
                                readerNomsEditeur.Close();

                                int idEditeur = 0;
                                bool okNomEditeur = false;

                                //  On boucle sur les noms des éditeurs déjà insérés dans la base
                                foreach (string nom in lesNomsEditeurs)
                                {
                                    //  Si l'éditeur du livre de la fiche est déjà présent dans la base
                                    if (editeur.ToLower() == nom)
                                    {
                                        okNomEditeur = true; //  On passe le bool à true
                                        break;
                                    }
                                }

                                //  Si le bool est à true, on récupère l'id de l'éditeur
                                if (okNomEditeur == true)
                                {
                                    selectIdEditeur.Parameters[0].Value = editeur;
                                    MySqlDataReader readerIdEditeur = selectIdEditeur.ExecuteReader();
                                    while (readerIdEditeur.Read())
                                    {
                                        idEditeur = Convert.ToInt32(readerIdEditeur["id"]);   //  On récupère l'id de l'auteur déjà inséré
                                    }
                                    readerIdEditeur.Close();
                                }
                                //  Sinon on insert l'éditeur dans la base
                                else
                                {
                                    //  insert table Editeur
                                    insertEditeur.Parameters[0].Value = editeur.ToLower();
                                    insertEditeur.ExecuteNonQuery();
                                    idEditeur = Convert.ToInt32(insertEditeur.LastInsertedId);  //  récupère l'id de l'éditeur que l'on vient d'insérer
                                }
                                //  Fin doublons éditeur

                                //  insert table Quizz
                                insertQuizz.Parameters[0].Value = idFiche;
                                insertQuizz.ExecuteNonQuery();

                                idQuizz = Convert.ToInt32(insertQuizz.LastInsertedId);  //  récupère l'id du quizz que l'on vient d'insérer

                                string nomFichier = Path.GetFileNameWithoutExtension(fiche);    //  récupère le nom du fichier

                                // inserer table livre
                                insertLivre.Parameters[0].Value = titre;
                                insertLivre.Parameters[1].Value = @"\\BABAORUM\photo\" + nomFichier + ".jpeg";
                                insertLivre.Parameters[2].Value = idAuteur;
                                insertLivre.Parameters[3].Value = idEditeur;
                                insertLivre.Parameters[4].Value = idQuizz;

                                insertLivre.ExecuteNonQuery();
                                etape = "questionProposition";  //  On passe au cas suivant
                            }
                            break;

                        case "questionProposition":
                            if (document.Paragraphs[i].Text.Contains("?") || document.Paragraphs[i].Text.Contains(":"))
                            {
                                insertAuteur.Parameters[0].Value = document.Paragraphs[i].Text;
                                string points = "";
                                int colonne = 2;
                                points = document.Tables[0].Rows[2].Paragraphs[colonne].Text;
                                //  insert table question
                                insertQuestion.Parameters[0].Value = document.Paragraphs[i].Text;
                                insertQuestion.Parameters[1].Value = points;
                                insertQuestion.Parameters[2].Value = idQuizz;
                                insertQuestion.ExecuteNonQuery();
                                idQuestion = Convert.ToInt32(insertQuestion.LastInsertedId);    //  On récupère l'id de la question que l'on vient d'insérer
                            }
                            else
                            {
                                // insert table proposition
                                insertProposition.Parameters[0].Value = document.Paragraphs[i].Text;
                                insertProposition.Parameters[1].Value = 0;
                                insertProposition.Parameters[2].Value = idQuestion;
                                insertProposition.ExecuteNonQuery();
                            }
                            break;
                    }   //fin switch
                }  //fin test si non ""
                i++;
            } // Le doc Word se ferme
            //Console.WriteLine("Insertion {0} terminée", fiche);
            rlConnexion.Close();
        }
    }
}
