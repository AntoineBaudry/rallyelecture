﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace rallyeLecture
{
    public class EventProgressionEventArgs : EventArgs
    {
        int valeur;
        public EventProgressionEventArgs(int valeur)
        {
            this.valeur = valeur;
        }

        public int Valeur
        {
            get { return valeur; }
            set { valeur = value; }
        }
    }

    delegate void DelegueSurValueChange(Object o, EventProgressionEventArgs e);
    class Chargement
    {
        public event DelegueSurValueChange ValueChange;
        protected void onValueChanged(int i)
        {
            if (ValueChange != null)
            {
                ValueChange(this, new EventProgressionEventArgs(i));
            }
        }
        public void traitement()
        {
            for (int i = 0; i <= 100; i++)
            {
                onValueChanged(i);
                Thread.Sleep(100);
            }
        }
    }
}
